Compose Service Role
=========

Role to install systemd service, which starts docker-compose app deployment

Requirements
------------

On target machine docker and docker-compose must be installed

Role Variables
--------------

`compose_path: /bin/docker-compose` - docker-compose binary path
`compose_service_workdir: /app` - path to folder, contains `docker-compose.yml`
`compose_service_name: docker-compose` - name for systemd service

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
  pre_tasks:
    - name: Copy docker-compose.yml
      template:
        path: templates/docker-compose.yml.j2
        dest: "{{ compose_service_workdir }}/docker-compose.yml"
        mode: '0644'


  roles:
    - compose_service
```

License
-------

BSD

