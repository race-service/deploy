# Deploy

## Getting Started

```bash
python3 -m venv .pyvenv
source .pyvenv/bin/activate
pip install -r requirements.txt

ansible-playbook compose.yml --ask-vault-pass
```